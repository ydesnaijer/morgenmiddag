/**
 * Created by yuri on 02/12/2017.
 */
const projects = [
    {
    id: 1,
    title: "Baardman",
    body: "Dit is de body van deze post. Hier staat normaal gesproken een verhaaltje over het project. Hier beschrijf ik hoe het tot stand gekomen is, welke technieken en/of materialen zijn gebruikt en wat het idee is.",
    createdAt: "2016-05-18 18:00:00.000",
    tags: ["illustratie", "muziek", "skateboard"],
    media: {
        href: "http://res.cloudinary.com/ox-company/image/upload/v1510511880/baardman_dytbnu.png",
        alt: "paars plaatje",
        type: "image"
        }
    },{
    id: 2,
    title: "Stoel bij raam",
    body: "Dit is de body van deze post. Hier staat normaal gesproken een verhaaltje over het project. Hier beschrijf ik hoe het tot stand gekomen is, welke technieken en/of materialen zijn gebruikt en wat het idee is.",
    createdAt: "2016-05-18 18:00:00.000",
    tags: ["illustratie", "3d-model", "skateboard"],
    media: {
        href: "http://res.cloudinary.com/ox-company/image/upload/v1510514490/stoel_bij_raam_aluqbg.jpg",
        alt: "paars plaatje",
        type: "image"
        }
    },{
    id: 3,
    title: "Virago aan de maas",
    body: "Dit is de body van deze post. Hier staat normaal gesproken een verhaaltje over het project. Hier beschrijf ik hoe het tot stand gekomen is, welke technieken en/of materialen zijn gebruikt en wat het idee is.",
    createdAt: "2016-05-18 18:00:00.000",
    tags: ["illustratie", "foto", "skateboard"],
    media: {
        href: "http://res.cloudinary.com/ox-company/image/upload/v1510514497/virago535_j3rqtr.jpg",
        alt: "paars plaatje",
        type: "image"
        }
    },{
    id: 4,
    title: "Mad Rocket Man",
    body: "Dit is de body van deze post. Hier staat normaal gesproken een verhaaltje over het project. Hier beschrijf ik hoe het tot stand gekomen is, welke technieken en/of materialen zijn gebruikt en wat het idee is.",
    createdAt: "2016-05-18 18:00:00.000",
    tags: ["illustratie", "game", "skateboard"],
    media: {
        href: "http://res.cloudinary.com/ox-company/image/upload/v1510514891/wallpaper_nidbmu.png",
        alt: "paars plaatje",
        type: "image"
        }
    },{
    id: 5,
    title: "Motoranimatie",
    body: "Dit is de body van deze post. Hier staat normaal gesproken een verhaaltje over het project. Hier beschrijf ik hoe het tot stand gekomen is, welke technieken en/of materialen zijn gebruikt en wat het idee is.",
    createdAt: "2016-05-18 18:00:00.000",
    tags: ["illustratie", "game", "skateboard"],
    media: {
        href: "https://res.cloudinary.com/ox-company/image/upload/v1512316685/thumbnail_tafmmj.png",
        alt: "paars plaatje",
        type: "image"
        }
    },{
    id: 6,
    title: "Mad Rocket Man",
    body: "Dit is de body van deze post. Hier staat normaal gesproken een verhaaltje over het project. Hier beschrijf ik hoe het tot stand gekomen is, welke technieken en/of materialen zijn gebruikt en wat het idee is.",
    createdAt: "2016-05-18 18:00:00.000",
    tags: ["illustratie", "game", "skateboard"],
    media: {
        href: "http://res.cloudinary.com/ox-company/image/upload/v1510514891/wallpaper_nidbmu.png",
        alt: "paars plaatje",
        type: "image"
        }
    }
];

export function getPosts(){
    return projects;
}
