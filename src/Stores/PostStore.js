/**
 * Created by yuri on 05/11/2017.
 */
import { EventEmitter } from 'events';
import Dispatcher from '../dispatcher';

class PostStore extends EventEmitter {

    addPost(title) {
        this.posts.push({
            id: (this.posts.length +1),
            title: title,
            description: "description"
        });

        this.emit('change');
    }

    getAll() {
        return this.posts;
    }

    handleActions(action) {
        switch (action.type){
            case "ADD_POST":
                this.addPost(action.title);
                break;
            case "LOADING_POSTS":
                console.log("fetching posts started");
                break;
            case "RECEIVE_POSTS":
                this.posts = action.data;
                this.emit('change');
                break;

            default:
                console.log("Event not recognized!")
        }
    }
}

const postStore = new PostStore();
Dispatcher.register(postStore.handleActions.bind(postStore));

window.dispatcher = Dispatcher;
export default postStore;