import React, {Component} from 'react';
import { Thumbnail, Modal, Button, Image } from 'react-bootstrap';
import Youtube from 'react-youtube';

export default class Post extends Component {
    constructor() {
        super();

        this.state = {
            showModal: false
        };

        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
    }
    close() {
        this.setState({
            showModal: false
        });
    }

    open() {
        this.setState({
            showModal: true
        });
    }
    render() {

        const post = this.props.post;

            return (
                <div>
                    <Thumbnail onClick={this.open} src={post.media.href} alt="242x200">
                        <h3>{post.title}</h3>
                    </Thumbnail>

                    <Modal bsSize="large" show={this.state.showModal} onHide={this.close}>
                        <Modal.Header closeButton>
                            <Modal.Title>{post.title}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Image src={post.media.href} responsive />
                            <p>{post.body}</p>

                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.close}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            )

    }
}