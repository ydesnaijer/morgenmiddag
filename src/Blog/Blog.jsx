import React, {Component} from 'react'
import Post from './Components/Post'
import PostStore from '../Stores/PostStore'
import * as PostActions from '../Actions/PostActions'
import { Col } from 'react-bootstrap'

export default class Blog extends Component {
    constructor() {
        super();

        this.state = {
            posts: []
        }
    }

    componentWillMount() {
        PostStore.on("change", () => {
            this.setState({
                posts: PostStore.getAll()
            });
        });

        PostActions.fetchPosts();
    }

    render() {
        const postsGrid = this.state.posts.map((post) => {
            return(
                // <Col xs={6} md={4}>
                <div className="block">
                    <Post post={post}/>
                </div>
                 // </Col>
             )
         });

        return (
            <div className="masonry">
                {/*<button onClick={this.fetchPosts.bind(this)}>add post</button>*/}
                {postsGrid}
            </div>
        )
    }
}