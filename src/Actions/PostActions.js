/**
 * Created by yuri on 05/11/2017.
 */
import Dispatcher from '../dispatcher';
import * as db from '../db';

export function addPost(title) {
    Dispatcher.dispatch({
        type: "ADD_POST",
        title: title
    })
}

export function deletePost(id) {
    Dispatcher.dispatch({
        type: "DELETE_POST",
        id: id
    })
}

export function fetchPosts() {
    Dispatcher.dispatch({type: "LOADING_POSTS"});


    Dispatcher.dispatch({
        type: "RECEIVE_POSTS",
        data: db.getPosts()
    });
}