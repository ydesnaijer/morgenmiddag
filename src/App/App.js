import React, { Component } from 'react';
import Blog from '../Blog/Blog'
import PrimaryNavigation from '../Components/PrimaryNavigation'

class App extends Component {
  render() {
    return (
      <div className="App">
        <PrimaryNavigation/>
        <Blog />
      </div>
    );
  }
}

export default App;
